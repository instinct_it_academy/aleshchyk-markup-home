function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

var userSurname = prompt('Введите Вашу фамилию:');
var userFirstName = prompt('Введите Ваше имя:');
var userPatronymic = prompt('Введите Ваше отчество:');
var userAge = prompt('Введите Ваш возраст:');
var userSex = confirm('Ваш пол Мужской?');
var isPensioner;

var userSexResult = userSex == true ? userSexResult = 'Мужской' : userSexResult = 'Женский';

var daysFromLeapYear = Math.floor(userAge / 4); 
var userAgeInDays = userAge * 365 + daysFromLeapYear;

if (userAge >= 63 && userSexResult == 'Мужской'){
  isPensioner = 'да';
} else if (userAge >= 56 && userSexResult == 'Женский'){
  isPensioner = 'да';
} else {
  isPensioner = 'нет';
}

alert(`ваше ФИО: ${capitalize(userSurname)} ${capitalize(userFirstName)} ${capitalize(userPatronymic)}\n
ваш возраст в годах: ${userAge}\n
ваш возраст в днях: ${userAgeInDays}\n
через 5 лет Вам будет: ${Number(userAge) + 5}\n
ваш пол: ${userSexResult}\n
вы на пенсии: ${isPensioner}`);